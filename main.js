/*

Описание Класса в README.md

*/

class NotEmptyFieldError extends Error {
    constructor(symbol, numOfField) {
        const message = `You can't put "${symbol}" on filed "${numOfField}"`;
        super(message);
        this.name = "NotEmptyFieldError";
        this.numOfField = numOfField;
    }
}

class InputTypeError extends Error {
    constructor(numOfField) {
        const message = `Incoming type must be "Number", not ${numOfField}}`;
        super(message);
        this.name = "InputTypeError";
        this.numOfField = numOfField;
    }
}

class IncorrectValueError extends Error {
    constructor(numOfField) {
        const message = `Incoming value must be greater or equal zero and less than nine, not ${numOfField}`;
        super(message);
        this.name = "IncorrectValueError";
        this.numOfField = numOfField;
    }
}

class TicTacToeLogic {
    PLAYER_X = "x";
    PLAYER_O = "o";
    DRAW = 'draw';

    isGameOver = false;
    currentPlayer = this.PLAYER_X;

    #field = [
        null, null, null,
        null, null, null,
        null, null, null,
    ];
    #emptyFields = 9;

    constructor(PLAYER_X = 'x', PLAYER_O = 'o') {
        // Заглушка на будущее
    }

    #checkIsEmptyField(numOfField) {
        return this.#field[numOfField] === null;
    }

    #putElement(symbol, numOfField) {
        if (isNaN(Number(numOfField))) {
            throw new InputTypeError(numOfField);
        }
        else if (numOfField < 0 || numOfField > 8) {
            throw new IncorrectValueError(numOfField);
        }
        else if (!this.#checkIsEmptyField(numOfField)) {
            throw new NotEmptyFieldError(symbol, numOfField);
        }
        this.#field[numOfField] = symbol;
        this.#emptyFields--;
        return true;
    }

    #compareCount(symbolX, symbolO) {
        if (symbolX === 3) {
            return this.PLAYER_X;
        }
        if (symbolO === 3) {
            return this.PLAYER_O;
        }
        return false
    }
    
    #resultObject(winner, combination = null) {
        const result = {
            winner: winner,
            combination: combination,
        }
        return result;
    }

    #checkHorisontalCombinations() {
        let symbolX;
        let symbolO;
        let combination;
        let resultCompareCount;
        for (let i = 0; i <= 8; i++) {
            if (i % 3 === 0) {
                symbolX = 0;
                symbolO = 0;
                combination = [];
            }
            switch (this.#field[i]) {
                case this.PLAYER_X:
                    symbolX++;
                    break;
                case this.PLAYER_O:
                    symbolO++;
                    break;
            }
            combination.push(i)
            resultCompareCount = this.#compareCount(symbolX, symbolO)
            if (resultCompareCount) {
                return this.#resultObject(resultCompareCount, combination);
            }
        }
        return false;
    }

    #checkVerticalCombinations() {
        let symbolX = 0;
        let symbolO = 0;
        let resultCompareCount;
        let combination = []
        for (let i = 0; i <= 2; i++) {
            for (let k = i; k <= i + 6; k+=3) {
                switch (this.#field[k]) {
                    case this.PLAYER_X:
                        symbolX++;
                        break;
                    case this.PLAYER_O:
                        symbolO++;
                        break;
                }
                combination.push(k);
            }
            resultCompareCount = this.#compareCount(symbolX, symbolO)
            if (resultCompareCount) {
                return this.#resultObject(resultCompareCount, combination);
            }
            symbolX = 0;
            symbolO = 0;
            combination = [];
        }
        return false;
    }

    #checkDiagonalCombitations() {
        let symbolX = 0;
        let symbolO = 0;
        let resultCompareCount;
        let combination = []
        for (let i = 0; i <= 8; i+=4) {
            switch (this.#field[i]) {
                case this.PLAYER_X:
                    symbolX++;
                    break;
                case this.PLAYER_O:
                    symbolO++;
                    break;
            }
            combination.push(i)
        }
        resultCompareCount = this.#compareCount(symbolX, symbolO);
        if (resultCompareCount) {
            return this.#resultObject(resultCompareCount, combination);
        }
        symbolX = 0;
        symbolO = 0;
        combination = [];
        for (let i = 2; i <= 6; i+=2) {
            switch (this.#field[i]) {
                case this.PLAYER_X:
                    symbolX++;
                    break;
                case this.PLAYER_O:
                    symbolO++;
                    break;
            }
            combination.push(i)
        }
        resultCompareCount = this.#compareCount(symbolX, symbolO);
        if (resultCompareCount) {
            return this.#resultObject(resultCompareCount, combination);
        }
        return false;
    }

    #checkRules() {
        let resultCheckVerticalCombinations = this.#checkVerticalCombinations();
        let resultChecklHorisontalCombinations = this.#checkHorisontalCombinations();
        let resultCheckDiagonalCombinations = this.#checkDiagonalCombitations();
        if (resultCheckVerticalCombinations) {
            this.isGameOver = true;
            return resultCheckVerticalCombinations;
        }
        else if (resultChecklHorisontalCombinations) {
            this.isGameOver = true;
            return resultChecklHorisontalCombinations;
        }
        else if (resultCheckDiagonalCombinations) {
            this.isGameOver = true;
            return resultCheckDiagonalCombinations;
        }
        else if (this.#emptyFields === 0) {
            this.isGameOver = true;
            return this.#resultObject(this.DRAW);
        }
        return false;
    }

    #changePlayerMove() {
        if (this.currentPlayer === this.PLAYER_X) {
            this.currentPlayer = this.PLAYER_O;
        }
        else {
            this.currentPlayer = this.PLAYER_X;
        }
    }

    viewField() {
        const field = this.#field;
        let message = '\n';
        for (let i = 0; i < 9; i++) {
            if (i % 3 === 0) {
                message += '\n';
            }
            if (field[i] === null) {
                message += ' _ ';
            }
            else {
                message += ` ${field[i]} `;
            }
        }
        return `${message}\n`;
    }
    
    game(numOfField) {
        this.#putElement(this.currentPlayer, numOfField);
        const afterMove = this.#checkRules();
        if (afterMove) {
            this.isGameOver = true;
            return afterMove;
        }
        this.#changePlayerMove();
        return false;
    }

    restart() {
        this.#field = [
            null, null, null,
            null, null, null,
            null, null, null,
        ];
        this.#emptyFields = 9;
        this.isGameOver = false;
        this.currentPlayer = this.PLAYER_X;
    }
}


function main() {
    const game = new TicTacToeLogic();
    while(!game.isGameOver) {
        let message = `Ход игрока "${game.currentPlayer}" ${game.viewField()}`
        try {
            const move = prompt(message);
            if (move === null) {
                if(confirm("Выход из игры?")) {
                    game.isGameOver = true;
                    break;
                }
                else {
                    continue;
                }
            }
            const resultOfMove = game.game(move);
            const field = game.viewField()
            if(resultOfMove) {
                switch(resultOfMove.winner) {
                    case game.PLAYER_X:
                        alert(`Победил игрок "${game.PLAYER_X}", комбинация [${resultOfMove.combination}]\n${field}`);
                        break;
                    case game.PLAYER_O:
                        alert(`Победил игрок "${game.PLAYER_O}", комбинация [${resultOfMove.combination}]\n${field}`);
                        break;
                    case game.DRAW:
                        alert(`Ничья!\n${field}`);
                }
                if(confirm('Начать заново?')) {
                    game.restart()
                }
            }
        }
        catch(error) {
            if (error instanceof InputTypeError) {
                alert(`Входящим значением должно быть число, не "${error.numOfField}"`);
            }
            else if (error instanceof NotEmptyFieldError) {
                alert(`Вы не можете поставить символ на поле "${error.numOfField}"`);
            }
            else if (error instanceof IncorrectValueError) {
                alert(`Входящим значением поля должно быть число от 0 до 8, не "${error.numOfField}"`)
            }
        }
    }
}

main();
