class NotEmptyFieldError extends Error {
    constructor(symbol, numOfField) {
        const message = `You can't put "${symbol}" on filed "${numOfField}"`;
        super(message);
        this.name = "NotEmptyFieldError";
        this.numOfField = numOfField;
    }
}

class InputTypeError extends Error {
    constructor(numOfField) {
        const message = `Incoming type must be "Number", not ${numOfField}}`;
        super(message);
        this.name = "InputTypeError";
        this.numOfField = numOfField;
    }
}

class IncorrectValueError extends Error {
    constructor(numOfField) {
        const message = `Incoming value must be greater or equal zero and less than nine, not ${numOfField}`;
        super(message);
        this.name = "IncorrectValueError";
        this.numOfField = numOfField;
    }
}
